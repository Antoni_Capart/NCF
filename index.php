<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>L'Agence Touriste</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />

	<!-- Bootstrap core CSS     -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />

	<!-- Animation library for notifications   -->
	<link href="assets/css/animate.min.css" rel="stylesheet"/>

	<!--  Light Bootstrap Table core CSS    -->
	<link href="assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>

	<!--  CSS for Demo Purpose, don't include it in your project     -->
	<link href="assets/css/demo.css" rel="stylesheet" />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
	<link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

	<script src="js/jquery-3.3.1.js"></script>
	<script src="js/messages.js"></script>
	<script src="js/pictures.js"></script>
	<script src="js/science-datas.js"></script>
	<script src="js/moms-messages.js"></script>
	<script src="js/clerentins-messages.js"></script>

	</head>
	<body>
		<div class="wrapper">
			<div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">
				<div class="sidebar-wrapper">
					<div class="logo">
						<a href="fsf.org" class="simple-text">L'agence Touriste</a>
					</div>
					<ul class="nav">
						<li class="active">
							<a href="index.php?page=messages.html">
								<i class="far fa-comment-alt"></i>
								<p>Message</p>
							</a>
						</li>
						<li class="active">
							<a href="index.php?page=picture.html">
								<i class="far fa-image"></i>
								<p>Pictures</p>
							</a>
						</li>
						<li class="active">
							<a href="index.php?page=mom.html">
								<i class="far fa-comment-alt"></i>
								<p>Mom's Messages</p>
							</a>
						</li>
						<li class="active">
							<a href="index.php?page=clerentin.html">
								<i><img src="Capture.PNG" style="widht: 35px; height: 35px;"/></i>
								<p>Clérentin's Messages</p>
							</a>
						</li>
						<li class="active">
							<a href="index.php?page=scientist.html">
								<i class="fas fa-flask"></i>
								<p>Données Scientifique</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="main-panel">
				    <div class="content">
					    <div class="container-fluid">

							<div class="row">
								<div class="card ">
									<?php
										if(isset($_GET['page']))$page = $_GET['page'];
										if (!empty($page)) {
											include('pages/' . $page);
										}else {include('pages/messages.html');}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>

	<!--   Core JS Files   -->
	<script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

	<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>

	<script src="js/moment.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){

	demo.initChartist();

});
</script>

</html>
