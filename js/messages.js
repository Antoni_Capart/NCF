function createMessagesTr(data){
    
    var tr = $('<tr>')
    tr.append($('<td>').text(data.content))
    tr.append($('<td>').text( moment(data.created).format('MMMM Do YYYY, h:mm:ss a') ))

    return tr
}
var id = 
$.ajax({
url: 'https://nightcode-phobos.cleverapps.io/api/messages/index?team_id=c9b96f32-dbc2-4adf-a7bc-7c53ee5fc1ed',
headers : {
    Accept : "application/json",
    "Content-Type" : "application/json",
    "X-API-Key" : 'cc8842778736782f48ec0c8f86a77d8e673a3c63aaaf9bd67cb29a2967ab1c96'
}
}).done(function(e) {
    for(var i = 0; i < e.data.length; i++){
        var tbody = $('#messages')
        tbody.append(createMessagesTr(e.data[i]))
    }
});